FROM php:5.6.32-apache-jessie

RUN apt-get update
RUN apt-get install -y g++ 
RUN apt-get install -y libpng-dev libmcrypt-dev libxml2-dev libtidy-dev

# xcache 
RUN curl -fsSL 'https://xcache.lighttpd.net/pub/Releases/3.2.0/xcache-3.2.0.tar.gz' -o xcache.tar.gz \
    && mkdir -p /tmp/xcache \
    && tar -xf xcache.tar.gz -C /tmp/xcache --strip-components=1 \
    && rm xcache.tar.gz \
    && docker-php-ext-configure /tmp/xcache --enable-xcache \
    && docker-php-ext-install /tmp/xcache \
    && rm -r /tmp/xcache

# install php mysql extension
RUN docker-php-ext-install mysql
RUN docker-php-ext-install pdo
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install gd
RUN docker-php-ext-install mcrypt
RUN docker-php-ext-install soap
RUN docker-php-ext-install tidy
RUN docker-php-ext-install xml
RUN docker-php-ext-install xmlrpc 

# config php.ini
COPY php.ini /usr/local/etc/php/php.ini
RUN sed -i 's/;date\.timezone.*/date.timezone = ASIA\/Bangkok/g' /usr/local/etc/php/php.ini
RUN sed -i 's/;upload_max_filesize.*/upload_max_filesize = 15M/g' /usr/local/etc/php/php.ini
RUN sed -i 's/;post_max_size.*/post_max_size = 15M/g' /usr/local/etc/php/php.ini
RUN sed -i 's/;short_open_tag.*/short_open_tag = On/g' /usr/local/etc/php/php.ini
RUN sed -i 's/;display_errors.*/display_errors = Off/g' /usr/local/etc/php/php.ini
#RUN a2enmod rewrite

# install zip archive extension
RUN docker-php-ext-install zip
RUN docker-php-ext-install calendar